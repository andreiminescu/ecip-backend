var express = require("express");
var app = express();
var index = __dirname + "/dist/quizUp/index.html";

//serve static file (index.html, images, css)
app.use(express.static(__dirname + '/dist/quizUp'));

app.get("/", function(request, response) {
  response.sendFile(index)
});

var port = process.env.PORT || 3000
app.listen(port, function() {
    console.log("http://localhost:" + port);
});
